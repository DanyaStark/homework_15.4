﻿#include <iostream>
#include <string>

int printNumbers(int a, int num1)
{
    if (a == 0)
    {
        for (int i = 0; i <= num1; i = i + 2)
        {
            std::cout << i << "\n";
        }
    }
    else if (a == 1)
    {
        for (int i = 1; i <= num1; i = i + 2)
        {
            std::cout << i << "\n";
        }
    }
    return 0;
}

int main()
{
    const int num = 10;

    for (int i = 0; i <= num; ++i)
    {
        std::cout << i << "\n";
    }

    int num1;
    int a;

    std::cout << "Enter number: ";
    std::cin >> num1;

    std::cout << "Even(0) or odd(1) numbers? ";
    std::cin >> a;

    printNumbers(a, num1);
}